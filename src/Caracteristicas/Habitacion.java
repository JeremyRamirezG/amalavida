/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caracteristicas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author maru
 */
public class Habitacion {
    //Atributos de la clase Habitacion
    private String idHabitacion;
    private String idHotel;
    private String tipoDeHabitacion;
    private double tarifaSencilla;
    private double tarifaDoble;
    private double tarifaTriple;
    public static ArrayList<Habitacion>habitacionesGlobales=new ArrayList<>();
    
    /**
     * Metodo que nos permite leer la informacion dad en el archivo habitaciones.csv
     * @throws FileNotFoundException
     * @throws IOException 
     */
    
    public static void load() throws FileNotFoundException, IOException{
        BufferedReader br3=null;
        try{
            br3=new BufferedReader(new FileReader("habitaciones.csv"));           
            br3.readLine(); 
            String line3= br3.readLine();
            while(null!=line3){
                String[] partesHabitacion=line3.split("\\|"); 
                String idHabitacion=partesHabitacion[0].toLowerCase();
                String idHotel=partesHabitacion[1].toLowerCase();
                String tipoDeHabitacion=partesHabitacion[2].toLowerCase();
                double tarifaSencilla=Double.parseDouble(partesHabitacion[3]);
                double tarifaDoble=Double.parseDouble(partesHabitacion[4]);
                double tarifaTriple=Double.parseDouble(partesHabitacion[5]);
                Habitacion h=new Habitacion(idHabitacion,idHotel,tipoDeHabitacion,tarifaSencilla,tarifaDoble,tarifaTriple);
                habitacionesGlobales.add(h);
                line3 = br3.readLine();
            }
        }catch (IOException e) {
          System.out.println("Error");
      } finally {
         if (null!=br3) {
            br3.close();    
        }}
    }
    
    /**
    *Constructor de la clase Habitacion
    *@param tipoHabitacion
    *@param tarifaSencilla
    * @param tarifaDoble
    * @param tarifaTriple
    */
    
    public Habitacion(String tipoDeHabitacion, double tarifaSencilla, double tarifaDoble, double tarifaTriple) {
        this.tipoDeHabitacion = tipoDeHabitacion;
        this.tarifaSencilla = tarifaSencilla;
        this.tarifaDoble = tarifaDoble;
        this.tarifaTriple = tarifaTriple;
    }
    
    /**
    *Constructor de la clase Habitacion
    * @param idHabitacion
    * @param idHotel
    *@param tipoHabitacion
    *@param tarifaSencilla
    * @param tarifaDoble
    * @param tarifaTriple
    */
    
    public Habitacion(String idHabitacion, String idHotel, String tipoDeHabitacion, double tarifaSencilla, double tarifaDoble, double tarifaTriple) {
        this.idHabitacion = idHabitacion;
        this.idHotel = idHotel;
        this.tipoDeHabitacion = tipoDeHabitacion;
        this.tarifaSencilla = tarifaSencilla;
        this.tarifaDoble = tarifaDoble;
        this.tarifaTriple = tarifaTriple;
    }
    
    //Getters and Setters de la clase
    public String getIdHabitacion() {
        return idHabitacion;
    }

    public void setIdHabitacion(String idHabitacion) {
        this.idHabitacion = idHabitacion;
    }

    public String getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(String idHotel) {
        this.idHotel = idHotel;
    }

    public String getTipoDeHabitacion() {
        return tipoDeHabitacion;
    }

    public void setTipoDeHabitacion(String tipoDeHabitacion) {
        this.tipoDeHabitacion = tipoDeHabitacion;
    }

    public double getTarifaSencilla() {
        return tarifaSencilla;
    }

    public void setTarifaSencilla(double tarifaSencilla) {
        this.tarifaSencilla = tarifaSencilla;
    }

    public double getTarifaDoble() {
        return tarifaDoble;
    }

    public void setTarifaDoble(double tarifaDoble) {
        this.tarifaDoble = tarifaDoble;
    }

    public double getTarifaTriple() {
        return tarifaTriple;
    }

    public void setTarifaTriple(double tarifaTriple) {
        this.tarifaTriple = tarifaTriple;
    }
    
}


