/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caracteristicas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author maru
 */
public class Catalogo {
    //Atributos de la clase Catalogo
     private String id_servicio;
    private String servicio;
    public static ArrayList<Catalogo>catalogosGlobales=new ArrayList<>();
    public ArrayList<Servicio> servicios = new ArrayList<>();
    
    
    //Lectura de Archivo de catalogo
    public static void load() throws FileNotFoundException, IOException{
         BufferedReader br=null;
         try{
            br=new BufferedReader(new FileReader("catalogo.csv"));   
            br.readLine();
            String line=br.readLine();
            while(null!=line){
                String[] partes=line.split("\\|");
                String id_servicio=partes[0].toLowerCase();
                String servicio=partes[1].toLowerCase();
                Catalogo p=new Catalogo(id_servicio,servicio);
                catalogosGlobales.add(p);
                line = br.readLine();    
            }
        }catch (Exception e) {
          System.out.println("Error");
      } finally {
         if (null!=br) {
            br.close();
            }
        }}

    
    //Creacion Constructor Catalogo y su sobrecarga
    
    public Catalogo(String servicio) {
        this.servicio = servicio;
    }
    
    public Catalogo(String id_servicio, String servicio) {
        this.id_servicio = id_servicio;
        this.servicio = servicio;
    }
//Getters y Setterss clase Catalogo
    public String getId_servicio() {
        return id_servicio;
    }

    public void setId_servicio(String id_servicio) {
        this.id_servicio = id_servicio;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }
    
    
}
