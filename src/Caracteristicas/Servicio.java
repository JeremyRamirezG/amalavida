/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caracteristicas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author maru
 */
public class Servicio {
    //Atributos Servicio
    private String secuencia;
    private String id_hotel;
    private String id_servicio;
    private String estado;
    
    
    
    /**
     *Lista catalogo que sera llenada con el id del servicio que tiene cada hotel 
    */
    public static ArrayList<Servicio> serviciosGlobales = new ArrayList<>();
    public ArrayList<Catalogo> catalogos = new ArrayList<>();
    
    //Lectura del archivo servicio
    public static void load() throws FileNotFoundException, IOException {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("servicios.csv"));
            br.readLine();
            String line = br.readLine();
            while (null != line) {
                String[] partes = line.split("\\|");
                String secuencia = partes[0].toLowerCase();
                String id_hotel = partes[1].toLowerCase();
                String id_servicio = partes[2].toLowerCase();
                String estado = partes[3].toLowerCase();
                Servicio p = new Servicio(secuencia, id_hotel, id_servicio, estado);
                serviciosGlobales.add(p);
                line = br.readLine();
            }
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            if (null != br) {
                br.close();
            }
        }
    }
    //Este metodo vincula los servicios con el catalago de servicio que tiene cada hotel
     public static void vincularCatalogo(ArrayList<Catalogo>catalogos) {
        for (Catalogo c : catalogos) {
            int idCat = Integer.parseInt(c.getId_servicio());
                Servicio h = get(idCat);
                h.catalogos.add(c);
        }
     }
     //Para obtner el id del servicio
        public static Servicio get(int id) {
        return serviciosGlobales.get(id - 1);
    }

        
        //Constructor clase Servicio
        public Servicio(String secuencia, String id_hotel, String id_servicio, String estado) {
        this.secuencia = secuencia;
        this.id_hotel = id_hotel;
        this.id_servicio = id_servicio;
        this.estado = estado;
    }
        
        //Getters y Setters clase 
    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(String id_hotel) {
        this.id_hotel = id_hotel;
    }

   
  public String getId_servicio() {
        return id_servicio;
    }
    public void setId_servicio(String id_servicio) {
        this.id_servicio = id_servicio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    

        
        
        
}
