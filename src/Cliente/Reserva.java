/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

/**
 *
 * @author User
 */
public class Reserva {
     private String entero;
    private String nombreHotel;
    private String tipohab;
    private double tarifaSenc;
    private double tarifadobl;
    private double tarifaTriple;
    private double tarifa;
    private String denominacion;
    private String fechaE;
    private String fechaS;
    private String ced;
    
    
    
    //Creacion de Constructor Reserva y sobrecarga de los mismos
    
     public Reserva(String entero, String nombreHotel, double tarifa) {
        this.entero = entero;
        this.nombreHotel = nombreHotel;
        this.tarifa = tarifa;
    }
   public Reserva(String ced,String entero,String nombreHotel,double tarifa,String fechaE,String fechaS){
   this.entero=entero;
   this.nombreHotel=nombreHotel;
   this.tarifa=tarifa;
   this.fechaE=fechaE;
   this.fechaS=fechaS;
  
   }
    

    public Reserva(String entero, String nombreHotel, String tipohab, double tarifaSenc, double tarifadobl, double tarifaTriple) {
        this.entero = entero;
        this.nombreHotel = nombreHotel;
        this.tipohab = tipohab;
        this.tarifaSenc = tarifaSenc;
        this.tarifadobl = tarifadobl;
        this.tarifaTriple = tarifaTriple;
        
        
        
    }


   
     public Reserva(String entero, String nombreHotel, String tipohab, double tarifaSenc) {
        this.entero = entero;
        this.nombreHotel = nombreHotel;
        this.tipohab = tipohab;
        this.tarifaSenc = tarifaSenc;
        
        
        
        
    }
    
     public Reserva(String entero, String nombreHotel,String denominacion, String tipohab, double tarifaSenc) {
        this.entero = entero;
        this.nombreHotel = nombreHotel;
        this.tipohab = tipohab;
        this.tarifaSenc = tarifaSenc;
        this.denominacion=denominacion;
        
        
        
        
    }
    
    
    //Creacion metodos Getters y Setters de sus respectivos atributos.
    
    public double getTarifa() {
        return tarifa;
    }

    public void setTarifa(double tarifa) {
        this.tarifa = tarifa;
    }
     public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }
     
    
    
    
    public String getEntero() {
        return entero;
    }

    public void setEntero(String entero) {
        this.entero = entero;
    }

    public String getNombreHotel() {
        return nombreHotel;
    }

    public void setNombreHotel(String nombreHotel) {
        this.nombreHotel = nombreHotel;
    }

    public String getTipohab() {
        return tipohab;
    }

    public void setTipohab(String tipohab) {
        this.tipohab = tipohab;
    }

    public double getTarifaSenc() {
        return tarifaSenc;
    }

    public void setTarifaSenc(double tarifaSenc) {
        this.tarifaSenc = tarifaSenc;
    }

    public double getTarifadobl() {
        return tarifadobl;
    }

    public void setTarifadobl(double tarifadobl) {
        this.tarifadobl = tarifadobl;
    }

    public double getTarifaTriple() {
        return tarifaTriple;
    }

    public void setTarifaTriple(double tarifaTriple) {
        this.tarifaTriple = tarifaTriple;
    }

    
    //Sobreescritura metodo toString()
    
    @Override
    public String toString() {
        return "Reserva{" + "entero=" + entero + ", nombreHotel=" + nombreHotel + ", tipohab=" + tipohab + ", tarifaSenc=" + tarifaSenc + ", tarifadobl=" + tarifadobl + ", tarifaTriple=" + tarifaTriple + '}';
    }
    
    
    
}
