/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import Lugar.Hotel;
import java.util.ArrayList;

/**
 *
 * @author maru
 */
public  abstract class Cliente {

    private String identificacion;
    private String ciudadResidencia;
    private String nombre;
    private String celular;
    private String telefono;
    private long tarjetaCredito;
    //private String cargo;
    //private String nombreGerente;
    private String email;
    private ArrayList<Hotel> revservas;

    public Cliente(String identificacion,String ciudadResidencia,String nombre,String celular,String telefono,long tarjetaCredito){
        this.identificacion=identificacion;
        this.ciudadResidencia=ciudadResidencia;
        this.nombre=nombre;
        this.celular=celular;
        this.telefono=telefono;
        this.tarjetaCredito=tarjetaCredito;
        this.revservas = null;
    }


    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCiudadResidencia() {
        return ciudadResidencia;
    }

    public void setCiudadResidencia(String ciudadResidencia) {
        this.ciudadResidencia = ciudadResidencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public long getTarjetaCredito() {
        return tarjetaCredito;
    }

    public void setTarjetaCredito(long tarjetaCredito) {
        this.tarjetaCredito = tarjetaCredito;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Hotel> getRevservas() {
        return revservas;
    }

    public void setRevservas(ArrayList<Hotel> revservas) {
        this.revservas = revservas;
    }

    
}


