/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

/**
 *
 * @author maru
 */
public class ClienteCorporativo extends Cliente{
    private String cargo;
    private String nombreGerente;
       
   public ClienteCorporativo(String identificacion,String ciudadResidencia,String nombre,String celular,String telefono,long tarjetaCredito,String cargo,String nombreGerente){
        super(identificacion,ciudadResidencia,nombre,celular,telefono,tarjetaCredito);
        this.cargo=cargo;
        this.nombreGerente=nombreGerente;

    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }
}

