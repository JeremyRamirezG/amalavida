/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import Caracteristicas.Habitacion;
import Lugar.Ciudad;
import Lugar.Hotel;
import Lugar.Provincia;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author maru
 */
public class Reservacion {
    private String establecimiento;
    private String tipoHabitacion;
    private int porcentajeReservaCliente;
    private String ciudad;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private Provincia p;
    private Ciudad c;
    private Hotel h;
    private Habitacion ha;
    
    public Reservacion(Provincia p,Ciudad c, Hotel h, Habitacion ha){
        this.p = p;
        this.c = c;
        this.h = h;
        this.ha = ha;
    }
    
    
    public Reservacion (){
    }
    

    public String getEstablecimiento() {
        
        return establecimiento;
    }

    public void setEstablecimiento(String establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    public void setTipoHabitacion(String tipoHabitacion) {
        this.tipoHabitacion = tipoHabitacion;
    }

    public int getPorcentajeReservaCliente() {
        return porcentajeReservaCliente;
    }

    public void setPorcentajeReservaCliente(int porcentajeReservaCliente) {
        this.porcentajeReservaCliente = porcentajeReservaCliente;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;

    }
    
    public String getCiudad(){
        return this.ciudad;
    }
    
    public void setCiudad(String ciudad){
        this.ciudad = ciudad;
    }
    
    public void mostrarReservacion(){
        System.out.println("*******Reservaciones*******");
        System.out.println("Establecimiento: "+this.establecimiento);
        System.out.println("Tipo de habitacion: "+this.tipoHabitacion);
        System.out.println("Fecha de inicio: "+this.checkIn.toString());
        System.out.println("Fecha de salida: "+this.checkOut.toString());
        System.out.println("***************************");
        
    }
}
