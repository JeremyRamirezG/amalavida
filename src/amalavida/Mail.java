/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amalavida;
import java.io.IOException;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;
/**
 *
 * @author maru
 */
public class Mail {
    
    public static void enviarEmail(String destinatario,String descripcion){
        String correoEnvia = "agen.ecuadorturismo@gmail.com";
	String claveCorreo = "0987203074";
        
        Properties props= new Properties();
        
        props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.port", "587");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.auth", "true");
	props.put("mail.user", correoEnvia);
	props.put("mail.password", claveCorreo);
        
	Session session = Session.getInstance(props, null);
        int aviso = 0;
        
	try {
		
	MimeMessage mimeMessage = new MimeMessage(session);
		 
	mimeMessage.setFrom(new InternetAddress(correoEnvia, "Agencia de Turismo Ecuador Ama La Vida"));
		    
	InternetAddress[] internetAddresses = {new InternetAddress(destinatario)};
		 
	mimeMessage.setRecipients(Message.RecipientType.TO,
	internetAddresses);
        
        mimeMessage.setSubject("Confirmacion de Reserva");
		 
	MimeBodyPart mimeBodyPart = new MimeBodyPart();
	mimeBodyPart.setText(descripcion);
       
        
        MimeBodyPart mimeBodyPartAdjunto = new MimeBodyPart();
	//mimeBodyPartAdjunto.attachFile(rutaImagen);
		 
	Multipart multipart = new MimeMultipart();
	multipart.addBodyPart(mimeBodyPart);

	//multipart.addBodyPart(mimeBodyPartAdjunto);

        mimeMessage.setContent(multipart);
		 
        Transport transport = session.getTransport("smtp");
	transport.connect(correoEnvia, claveCorreo);
	transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
		   
	transport.close();
	}catch (IOException | MessagingException ex) {
	ex.printStackTrace();
		   JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
		   aviso = 1;
		  } 
    }
}
