/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amalavida;

import Caracteristicas.Catalogo;
import Caracteristicas.Habitacion;
import Caracteristicas.Servicio;
import Cliente.Cliente;
import Cliente.ClienteCorporativo;
import Cliente.ClienteTurista;
import Cliente.Reserva;
import Cliente.Reservacion;
import Lugar.Ciudad;
import Lugar.Provincia;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class Bot {
    
    //Atributos clase Bot
    private ArrayList<ClienteTurista>clientesturistas = new ArrayList<>();
    private ArrayList< ClienteCorporativo>clientescorporativos = new ArrayList<>();
    private ArrayList<Cliente> clientes = new ArrayList<>();
    static Scanner scc= new Scanner(System.in);
    private String consulta;
    public static ArrayList<Ciudad> ciudades;
    public static ArrayList<Provincia> provincias;
    public static ArrayList<Catalogo>catalogos;
    public static ArrayList<Habitacion> habitaciones;
    private List<String> listaPalabras=Arrays.asList("reservar", "reserva","reservacion","cancelar","cancelo","cancelacion","informacion","salir");
    private static String ciudad;
    private static String ingresociudad;//Obtengo id de la cidad
    public static double num;
    public static double numMaximo;
    public static double numMinimo;
    public static String idProvincia1;
    private static String id;//id para iformacion
    private static String requerimiento;//es para obtener el id del servicio requerido
    private static String palabra;//solo es para validar lo que ingresa el usuario
    private static String infoHotel;//informacion para hoteles
    private ArrayList<String>inforeserva=new ArrayList<>();
    private static String[]in=null;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private ArrayList<Reserva>rsxd=new ArrayList<>();
    private ArrayList<Reserva>fast=new ArrayList<>();
    
    static Scanner sc2=new Scanner(System.in);
    /**
     * Metodo de lectura del Archivo catalogo.csv
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public  void lecturaDeArchivoCatalogo() throws FileNotFoundException, IOException{
         BufferedReader br=null;
         try{
            br=new BufferedReader(new FileReader("catalogo.csv"));   
            br.readLine();
            String line=br.readLine();
             catalogos=new ArrayList();
            while(null!=line){
                String[] partes=line.split("\\|");
                String id_servicio=partes[0].toLowerCase();
                String servicio=partes[1].toLowerCase();
                Catalogo p=new Catalogo(id_servicio,servicio);
                catalogos.add(p);
                line = br.readLine();    
            }
        }catch (Exception e) {
          System.out.println("Error");
      } finally {
         if (null!=br) {
            br.close();
            }
        }}
    
    /**
     * Metodo de lectura del archivoPorvincia
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void lecturaDeArchivoProvincias() throws FileNotFoundException, IOException{
        BufferedReader br=null;
        try{
            br=new BufferedReader(new FileReader("provincias.csv"));   
            br.readLine();
            String line=br.readLine();
            provincias=new ArrayList();
            while(null!=line){
                String[] partesProvincias=line.split("\\|");
                String idProvincia=partesProvincias[0].toLowerCase();
                String provincia=partesProvincias[1].toLowerCase();
                String descripcion=partesProvincias[2].toLowerCase();
                String region=partesProvincias[3].toLowerCase();
                String web=partesProvincias[4].toLowerCase();
                Provincia p=new Provincia(idProvincia,provincia,descripcion,region,web);
                provincias.add(p);
                line = br.readLine();    
            }
        }catch (Exception e) {
          System.out.println("Error");
      } finally {
         if (null!=br) {
            br.close();
            }
        }
    }
    
    /**
     * Metodo de lectura de ArchivoCiudades
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void lecturaDeArchivosCiudades() throws FileNotFoundException, IOException{
        BufferedReader br2=null;
        try{
            br2=new BufferedReader(new FileReader("ciudades.csv"));    
            br2.readLine();
            String line2=br2.readLine();  
            ciudades=new ArrayList();
            while(null!=line2){
                String[] partesCiudades=line2.split("\\|"); 
                String idProvincias=partesCiudades[0].toLowerCase();
                String idCiudades=partesCiudades[1].toLowerCase();
                String ciudad=partesCiudades[2].toLowerCase();
                Ciudad c=new Ciudad(idProvincias,idCiudades,ciudad);
                ciudades.add(c);
                line2 = br2.readLine();}  
        }catch (Exception e) {
          System.out.println("Error");
      } finally {
         if (null!=br2) {
            br2.close();
            }
        
        }
        
    }
    
    /**
     * Metodo para la lectura de archivo habitaciones
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void lecturaDeArchivosHabitacion() throws FileNotFoundException, IOException{
        BufferedReader br3=null;
        try{
            br3=new BufferedReader(new FileReader("habitaciones.csv"));           
            br3.readLine(); 
            String line3= br3.readLine();
            habitaciones=new ArrayList();
            while(null!=line3){
                String[] partesHabitacion=line3.split("\\|"); 
                String idHabitacion=partesHabitacion[0].toLowerCase();
                String idHotel=partesHabitacion[1].toLowerCase();
                String tipoDeHabitacion=partesHabitacion[2].toLowerCase();
                double tarifaSencilla=Double.parseDouble(partesHabitacion[3]);
                double tarifaDoble=Double.parseDouble(partesHabitacion[4]);
                double tarifaTriple=Double.parseDouble(partesHabitacion[5]);
                Habitacion h=new Habitacion(idHabitacion,idHotel,tipoDeHabitacion,tarifaSencilla,tarifaDoble,tarifaTriple);
                habitaciones.add(h);
                line3 = br3.readLine();
            }
        }catch (IOException e) {
          System.out.println("Error");
      } finally {
         if (null!=br3) {
            br3.close();    
        }}
        }
    /**
     * Metodo para realizar la interaccion con el cliente
     * @param cliente
     * @throws IOException 
     */
     public void opcionUsuario(Cliente cliente) throws IOException {
        boolean cond0 = false;
        while (!cond0) {
            boolean salida = false;
            boolean condicion = false;
            System.out.println("Hola soy Jeremy, tu agente asignado! ¿Deseas una de las siguientes opciones"
                    + "\n------RESERVAR----- "
                    + "\n-----INFORMACION----- "
                    + "\n-----CANCELAR-----"
                    + "\n-----SALIR-----"
                    + "\nIngrese solo una opcion: ");
            while (!condicion) {
                String ingresoUsuario = sc2.nextLine().toLowerCase();
                boolean respuesta = listaPalabras.contains(ingresoUsuario);
                if (respuesta == true) {
                    palabra = ingresoUsuario;
                    condicion = true;
                    //respuestaU=ingresoUsuario;
                } else {
                    System.out.println("Opcion Incorrecta. Ingrese de nuevo lo que desea realizar: ");
                    condicion = false;
                }
            }

            boolean cond = false;
            while (!cond) {
                boolean seguir = false;
                switch (palabra) {
                    case "reservar":
                        opcionProvincia();
                        opcionCiudad();
                        requerimeintoEspecial();
                        rangoPrecio(cliente);
                        break;
                    case "informacion":
                        System.out.println("Para la informacion del hotel que usted desea necesito saber"
                                + "la provincia y la ciudad donde se encuentra el hotel");
                        opcionProvincia();
                        opcionCiudad();
                        //requerimeintoEspecial();
                        informacionHotel();
                        System.out.println("Desea serguir obteniendo informacion digite S si desea seguir"
                                + "o N para el menu principal");
                        String respuesta = sc2.nextLine().toUpperCase();
                        switch (respuesta) {
                            case "S":
                                informacionHotel();
                                break;
                            default:
                                seguir = false;
                        }
                        case "salir":
                            cond0=true;
                            break;
            }break;

                
        }
    }}
        
       
     /**
      * Metodo para la seleccion de la provincia que se desea buscar
      * @throws IOException 
      */
     public  void opcionProvincia() throws IOException{
        lecturaDeArchivoProvincias();
        boolean condicion=false;
        System.out.print("Ingrese la provincia en la que desea realizar su reserva: ");
        while(!condicion){
            String ingresoProvincia=sc2.nextLine().toLowerCase();
            boolean respuesta=false;
            for(Provincia p: provincias){
                if((p.getProvincia()).equals(ingresoProvincia)){;
                    idProvincia1=p.getIdProvincia();
                    respuesta=true;
                }
            }if(respuesta!=true){
                System.out.print("Provincia no disponible. Por favor ingrese otra: ");
            }else{
                condicion=true;
            }
        }}
     
     /**
      * Metodo para interactuar con el usuario y conocer la ciudad de su preferencia
      * @throws IOException 
      */
    public void opcionCiudad() throws IOException{
        lecturaDeArchivosCiudades(); 
        lecturaDeArchivoProvincias();
        boolean condicion=false;
        System.out.print("Ingrese la ciudad en la que desea realizar su reserva: ");
        while(!condicion){ 
            String ingresoCiudad=sc2.nextLine().toLowerCase();
            boolean respuesta=false;
            for(Ciudad c: ciudades){
                if((c.getCiudad()).equals(ingresoCiudad)){
                    ingresociudad=c.getIdCiudad();
                    ciudad=c.getCiudad();
                    String idProvincia=c.getIdProvincia();
                    if(idProvincia1.equals(idProvincia)){
                            respuesta=true;
                        }
                    }
            }if(respuesta!=true){
                System.out.print("Ciudad No Disponible o No Perteneciente a la Provincia Indicada. Por favor ingrese otra: ");
            }else{
                condicion=true;
            }
        }
    }
    
    /**
     * Metodo para validar el ingreso de un double 
     */
    public void validarDouble() {
        boolean esDouble = false;
        num = -1;
        do {
            String valor = sc2.nextLine();
            try {
                num = Double.parseDouble(valor);
                esDouble = true;
            } catch (NumberFormatException nfe) {
                System.out.print("No ingresó una cantidad. Por favor escriba de nuevo su valor: ");
            }
        } while (!esDouble);
    }
    
    /**
     * Metodo para poder seleccionar los hoteles en base a los requerimientos especiales del usuario
     * @throws IOException 
     */
    public void requerimeintoEspecial()throws IOException{
        lecturaDeArchivoCatalogo();
        boolean condicion=false;
        System.out.println("¿Deseas algún requerimiento en especial?");
        while(!condicion){
         String respuesta=sc2.nextLine().toLowerCase();
         if(respuesta.equals("no")){
             condicion=true;
         }
         else{
          boolean in=false;
          String[] cadena=respuesta.split(" ");
          for(String p:cadena){
            for(Catalogo c:catalogos){
             if(c.getServicio().equals(p)){
                 requerimiento=c.getId_servicio();
                 in=true;}}}
         if(in!=true){
             System.out.println("El requermiento que usted desea no esta"
                     + "disponible. Por favor digite otro ");
         }
         else{
             condicion=true;
        }}}
    } 
    
    /**
     * Metodo utilizado para poder validar el rango de precio de preferencia del usuario
     * @param user
     * @throws IOException 
     */
    public void rangoPrecio(Cliente user) throws IOException{
        int id=0;
        int id2=0;
        String entero="";
        
        System.out.println("Ahora Ingresaremos el Rango de Precio");
        System.out.print("Ingrese su precio minimo: ");
        validarDouble();
        numMinimo=num;
        System.out.print("Ingrese su precio máximo: ");
        validarDouble();
        numMaximo=num;
        Ciudad.load();
        Lugar.Hotel.load();
        Habitacion.load();
        Servicio.load();
        Catalogo.load();
        Ciudad.vincularHoteles(Lugar.Hotel.hotelesGlobales);
        Lugar.Hotel.vincularHabitaciones(Habitacion.habitacionesGlobales);
        Lugar.Hotel.vincularServicios(Servicio.serviciosGlobales);
        Servicio.vincularCatalogo(Catalogo.catalogosGlobales);
        System.out.println("Bot:Seguro los hoteles que encontramos en "+ciudad.toUpperCase()+" para ti son: ");
        for (Lugar.Hotel h : Ciudad.get(Integer.parseInt(ingresociudad)).hoteles){
            
            for(Servicio s:Lugar.Hotel.get(Integer.parseInt(h.getIdHotel())).servicios){
                
                if(s.getId_servicio().equals(requerimiento)){
                  
                   for(Catalogo ca:Servicio.get(Integer.parseInt(requerimiento)).catalogos){
                       
                         for(Habitacion c:h.habitaciones){
                               if((c.getTarifaSencilla()>=numMinimo&&c.getTarifaSencilla()<=numMaximo)){
                                   id+=1;
                                   System.out.println(id+" "+h.getNombreHotel());
                                   
                                   entero=Integer.toString(id);
                                   
                                   Reserva r = new Reserva(entero,h.getNombreHotel(),"Habitacion Sencilla",c.getTipoDeHabitacion(),c.getTarifaSencilla());       
                                      rsxd.add(r);
                               System.out.println("        "+"Habitacion Sencilla  "+c.getTipoDeHabitacion()+"  "+c.getTarifaSencilla());
                               }if((c.getTarifaDoble()>=numMinimo&&c.getTarifaDoble()<=numMaximo)){
                                   
                                   Reserva r = new Reserva(entero,h.getNombreHotel(),"Habitacion doble ",c.getTipoDeHabitacion(),c.getTarifaDoble());       
                                      rsxd.add(r);
                                   
                               System.out.println("        "+"Habitacion Doble "+c.getTipoDeHabitacion()+"  "+c.getTarifaDoble());
                               }if((c.getTarifaTriple()>=numMinimo&&c.getTarifaTriple()<=numMaximo)){
                                   Reserva r = new Reserva(entero,h.getNombreHotel(),"Habitacion triple",c.getTipoDeHabitacion(),c.getTarifaTriple());       
                                      rsxd.add(r);
                                   
                               System.out.println("        "+"Habitacion Triple  "+c.getTipoDeHabitacion()+"  "+c.getTarifaTriple());
                               
                        }
                          }

                                         }

                                   }}}
                System.out.println("Ingrese el hotel que desea: ");
                String numfast="";
                String entero2="";         
                String rs=sc2.nextLine();

                for(Reserva q:rsxd){

                    if(rs.equals(q.getEntero())){

                        id2+=1;

                        System.out.println(id2+" "+" "+q.getNombreHotel()+" "+q.getDenominacion()+" "+q.getTipohab()+" con precio: "+q.getTarifaSenc()+"$");
                                entero2=Integer.toString(id2);
                        Reserva rd=new Reserva(entero2,q.getNombreHotel(),q.getDenominacion(),q.getTipohab(),q.getTarifaSenc());
                        fast.add(rd);

                    }

                }System.out.println("Seleccione la habitacion que desea");
                String rs2=sc2.nextLine();

                for(Reserva ty:fast){
                //
                if(rs2.equals(ty.getEntero())){
                //System.out.println(ty);
                String email = "";
                    System.out.print("Ingrese su email para confirmar su reserva: ");
                Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
                // El email a validar
                boolean condicion = false;
                    while(!condicion){
                        email = sc2.nextLine();;
                        Matcher mather = pattern.matcher(email);
                        if (mather.find() == true) {
                            String descripcionFinal="***RESERVACION CONFIRMADA***"+"\nNombre del Hotel: "+ty.getNombreHotel()+"\nTipo de Habitacion: "+ty.getDenominacion()+"\nNombre de la Habitacion: "+ty.getTipohab()+"\nTarifa a pagar:"+ty.getTarifaSenc()+"$"+"\nGRACIAS POR PREFERIRNOS";
                            Mail.enviarEmail(email, descripcionFinal);
                            condicion=true;
                            break;
                        } else {
                            System.out.print("El email ingresado es inválido. Por favor intenta de nuevo: ");
                        }   
            }
                System.out.println("Perfecto! Usted ha reservado lo siguiente: ");
                   
                   System.out.println(ty.getNombreHotel()+","+" "+ty.getDenominacion()+" "+ty.getTipohab()+" "+ty.getTarifaSenc()+"$");

                }

                }

                    }
        
    /**
     * Metodo dado para obtener cierta informacion acerca de un hotel
     * @throws IOException 
     */
    public void informacionHotel() throws IOException{
         lecturaDeArchivosCiudades(); 
         lecturaDeArchivoProvincias();
          Ciudad.load();
          Lugar.Hotel.load();
          Habitacion.load();
          Servicio.load();
          Catalogo.load();
          
          Ciudad.vincularHoteles(Lugar.Hotel.hotelesGlobales);
          Lugar.Hotel.vincularHabitaciones(Habitacion.habitacionesGlobales);
          Lugar.Hotel.vincularServicios(Servicio.serviciosGlobales);
          Servicio.vincularCatalogo(Catalogo.catalogosGlobales);
         for (Lugar.Hotel h : Ciudad.get(Integer.parseInt(ingresociudad)).hoteles){
             System.out.println("Id: "+h.getIdHotel()+"  " +h.getNombreHotel());
         }
          System.out.println("Si desea informacion de uno de ellos solo tiene que digitar el id que se encuentra"
                 + "en la parte izquiera del Hotel");
         boolean condicion=false;
         while(!condicion){
             String info=sc2.nextLine();
             boolean respuesta=false;
              Ciudad.load();
              Lugar.Hotel.load();
              Ciudad.vincularHoteles(Lugar.Hotel.hotelesGlobales);
              for (Lugar.Hotel h : Ciudad.get(Integer.parseInt(ingresociudad)).hoteles){
                  if(info.equals(h.getIdHotel())){
                      infoHotel=info;
                      respuesta=true;
                  }
              }
             if(respuesta!=true){
                 System.out.println("El id ingresaro es incorecto. Digitelo de nuevo:");
             }
             else{
                 condicion=true;
             }
         }
        ArrayList<String>hotel=new ArrayList<>();

          for (Lugar.Hotel h : Ciudad.get(Integer.parseInt(ingresociudad)).hoteles) {
            for (Habitacion c : h.habitaciones) {
                if (h.getIdHotel().equals(infoHotel)) {
                    if (!hotel.contains(h.getNombreHotel())) {
                        hotel.add(h.getNombreHotel());
                        hotel.add(h.getDescripcion());
                        hotel.add(h.getDireccion());
                        hotel.add(h.getTarjeta());
                        hotel.add(h.getWeb());}}}}    

        for (Lugar.Hotel h : Ciudad.get(Integer.parseInt(ingresociudad)).hoteles) {
            if (h.getIdHotel().equals(infoHotel)){
                 for (Habitacion c : h.habitaciones) {
                if (h.getIdHotel().equals(infoHotel)) 
                    if (!hotel.contains(c.getTipoDeHabitacion())) {
                        hotel.add(c.getTipoDeHabitacion());
                        hotel.add(Double.toString(c.getTarifaSencilla()));
                        hotel.add(Double.toString(c.getTarifaDoble()));
                        hotel.add(Double.toString(c.getTarifaTriple()));
                        }}}
            }
//            for (Habitacion c : h.habitaciones) {
//                if (h.getIdHotel().equals(infoHotel)) 
//                    if (!hotel.contains(c.getTipoDeHabitacion())) {
//                        hotel.add(c.getTipoDeHabitacion());
//                        hotel.add(Double.toString(c.getTarifaSencilla()));
//                        hotel.add(Double.toString(c.getTarifaDoble()));
//                        hotel.add(Double.toString(c.getTarifaTriple()));
//                        }}}
        
        for (Servicio s : Lugar.Hotel.get(Integer.parseInt(infoHotel)).servicios) {
            for (Catalogo ca : Servicio.get(Integer.parseInt(s.getId_servicio())).catalogos) {
                if (ca.getId_servicio().equals(s.getId_servicio())) {
                    if (!hotel.contains(ca.getServicio())) {
                        hotel.add(ca.getServicio());}}}}
        for(String p:hotel){
            System.out.println(p);
        }
    }

    /**
     * Metodo para el registro de un Cliente de tipo turista
     */
    
    public void RegistrarClienteTurista(){
        System.out.println("Ingrese identificacion: ");
                        String identificacion=scc.nextLine();
                        System.out.println("Ingrese ciudad de Residencia: ");
                        String ciudadResidencia=scc.next();
                        System.out.println("Ingrese nombre: ");
                        String nombre=scc.next();
                        System.out.println("Ingrese celular: ");
                        String celular=scc.next();
                        System.out.println("Ingrese telefono: ");
                        String telefono=scc.next();
                        System.out.println("Ingrese tarjeta de Credito: "); 
                        long tarjetaCredito=scc.nextLong();
                        ClienteTurista c1=new ClienteTurista(identificacion,ciudadResidencia,nombre,celular,telefono,tarjetaCredito);
                        clientesturistas.add(c1);
                        clientes.add(c1);

        
    }
    
    /**
     * Metodo para registrar un cliente de tipo coorporativo
     */
    public void RegistrarClienteCorporativo(){
        System.out.println("Ingrese identificacion: ");
                        String identificacion1=scc.nextLine();
                        scc.nextLine();
                        System.out.println("Ingrese ciudad de Residencia: ");
                        String ciudadResidencia1=scc.next();               
                        System.out.println("Ingrese nombre: ");
                        String nombre1=scc.next();                     
                        System.out.println("Ingrese celular: ");
                        String celular1=scc.next();
                        System.out.println("Ingrese telefono: ");
                        String telefono1=scc.next();
                        System.out.println("Ingrese tarjeta de Credito: "); 
                        long tarjetaCredito1=scc.nextLong();
                        System.out.println("Ingrese cargo: ");
                        String cargo=scc.next();
                        System.out.println("Ingrese nombre gerente: ");
                        String nombreGerente=scc.next();
                        ClienteCorporativo c2=new ClienteCorporativo(identificacion1,ciudadResidencia1,nombre1,celular1,telefono1,tarjetaCredito1,cargo,nombreGerente);
                        clientescorporativos.add(c2);
                        clientes.add(c2);
                         for (ClienteCorporativo cc :clientescorporativos){
                            System.out.println(cc.getNombre()+cc.getNombreGerente());
                        
                        
                        
                        
    }
    }                 
    /**
     * Metodo para validar el registro del Cliente
     * @throws IOException 
     */
                public void ValidarRegistroCliente() throws IOException{
                
                    System.out.println("¿Ya se encuentra registrado?: ");
                    String respc=sc2.nextLine().toLowerCase();                   
                    if(respc.equals("no")){                   
                    RegistrarClienteTurista();
                    }else {
                        System.out.println("Ingrese su numero de cedula: ");
                        String cedula=sc2.nextLine();
                        ClienteTurista clientet = obtnerCliente(cedula);
                        opcionUsuario(clientet);
                        //rangoPrecio(clientet);
                    }

                }  
                
          /**
           * Metodo para obtener el cliente
           * @param cedula
           * @return 
           */      
       private ClienteTurista obtnerCliente(String cedula){
           for(ClienteTurista c: clientesturistas){
               if(c.getIdentificacion().equals(cedula))
                   return c;
           }
           return null;
       }     
       /**
        * Metodo par obtener el hotel requerido
        * @param nombre
        * @return 
        */
       private Lugar.Hotel obtenerHotel(String nombre){
           for(Lugar.Hotel h: Lugar.Hotel.hotelesGlobales){
               if(h.getNombreHotel().toLowerCase().equals(nombre)){
                   System.out.println(h);;
               }
           }
           return null;
       }
       
       /**
        * Metodo Mostrar Reservaciones
        * @param reservaciones 
        */
      public void mostrarVentanaAgencia(ArrayList<Reservacion> reservaciones) {
        Scanner sc = new Scanner(System.in);
        Scanner sc1 = new Scanner(System.in);

        System.out.println("1.-Consultar reservas\n2.-Salir\n3.-Digite opcion ");
        int ingresoAgencia = sc.nextInt();

        switch (ingresoAgencia) {
            case 1:
                System.out.println("Ingrese fecha de inicio(dd/mm/yyyy):");
                String checkin = sc1.next();
                LocalDate checkinD = LocalDate.parse(checkin, formatter);
                System.out.println("Ingrese una fecha de fin(dd/mm/yyyy):");
                String checkout = sc1.next();
                LocalDate checkoutD = LocalDate.parse(checkout, formatter);
                System.out.println("Ingrese una ciudad:");
                String ingresoCiudad = sc1.next();
                int contador = 0;
                for (Reservacion r : reservaciones) {
                    if (r.getCheckIn().isAfter(checkinD) && r.getCheckOut().isBefore(checkoutD) && ingresoCiudad.equals("T")) {

                        r.mostrarReservacion();
                        contador += 1;

                    } else if (r.getCheckIn().isAfter(checkinD) && r.getCheckOut().isBefore(checkoutD) && r.getCiudad().equals(ingresoCiudad)) {
                        r.mostrarReservacion();
                        contador += 1;
                    } else {
                        System.out.println("no existen reervaciones");
                    }
                }
                System.out.println("se han encontrado " + contador + " reservaciones");
        }
    }                   
                         
    
}
