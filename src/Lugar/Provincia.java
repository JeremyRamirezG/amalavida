/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lugar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author maru
 */
public class Provincia {
    //Atributos de la clase provincia
    public String idProvincia;
    public String provincia;
    public String descripcion;
    public String region;
    public String web;
    public static ArrayList<Provincia> todos = new ArrayList<>();
    
    /**
     * Metodo para la lectura del archivo provincias.csv
     * @throws FileNotFoundException
     * @throws IOException 
     */
    
    public static void load() throws FileNotFoundException, IOException{
         BufferedReader br=null;
         try{
            br=new BufferedReader(new FileReader("provincias.csv"));   
            br.readLine();
            String line=br.readLine();
            while(null!=line){
                String[] partesProvincias=line.split("\\|");
                String idProvincia=partesProvincias[0].toLowerCase();
                String provincia=partesProvincias[1].toLowerCase();
                String descripcion=partesProvincias[2].toLowerCase();
                String region=partesProvincias[3].toLowerCase();
                String web=partesProvincias[4].toLowerCase();
                Provincia p=new Provincia(idProvincia,provincia,descripcion,region,web);
                todos.add(p);
                line = br.readLine();    
            }
        }catch (Exception e) {
          System.out.println("Error");
      } finally {
         if (null!=br) {
            br.close();
            }
        }
        
    }
    
    /**
     * Constructor de Provincia, el cual recibe los siguientes parametros
     * @param idProvincia
     * @param provincia
     * @param descripcion
     * @param region
     * @param web 
     */
    public Provincia(String idProvincia, String provincia, String descripcion, String region, String web) {
        this.idProvincia = idProvincia;
        this.provincia = provincia;
        this.descripcion = descripcion;
        this.region = region;
        this.web = web;
    }
    //Getters and Setters de la clase
    
    public String getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(String idProvincia) {
        this.idProvincia = idProvincia;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }
    
}

