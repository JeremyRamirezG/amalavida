/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lugar;

import Caracteristicas.Habitacion;
import Caracteristicas.Servicio;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Hotel {
    private String idHotel;
    private String idCiudad;
    private String nombreHotel;
    private String descripcion;
    private String tarjeta;
    private String ubicacion;
    private String direccion;
    private String web;
    private String clasificacionHotel;
    private String fotoHotel;
    private String latitud;
    private String longitud;
    public static ArrayList<Hotel>hotelesGlobales=new ArrayList<>();
    /**
     * Se crean dos listas una de habitacion y servicios ya que ambas estan
     * conectadas con hoteles mediante su Id, para lugo ser llenadas con 
     * con id de cada hotel
     */
    public ArrayList<Habitacion> habitaciones = new ArrayList<>();
    public ArrayList<Servicio>servicios=new ArrayList<>();
    
    public Hotel(String idHotel, String idCiudad, String nombreHotel, String descripcion, String tarjeta, String ubicacion, String direccion, String web, String clasificacionHotel, String fotoHotel, String latitud, String longitud) {
        this.idHotel = idHotel;
        this.idCiudad = idCiudad;
        this.nombreHotel = nombreHotel;
        this.descripcion = descripcion;
        this.tarjeta = tarjeta;
        this.ubicacion = ubicacion;
        this.direccion = direccion;
        this.web = web;
        this.clasificacionHotel = clasificacionHotel;
        this.fotoHotel = fotoHotel;
        this.latitud = latitud;
        this.longitud = longitud;
    }
    //Lectura del archivo hoteles, para proceder a llenar la lista de hotelesGlobales
        public static void load() throws FileNotFoundException, IOException{
        BufferedReader br3=null;
        try{
            br3=new BufferedReader(new FileReader("hoteles1.csv"));           
            br3.readLine(); 
            String line3= br3.readLine();
            while(null!=line3){
                String[] partes=line3.split("\\|");
                String idHotel = partes[0];
                String idCiudad = partes[1];
                String nombreHotel = partes[2];
                String descripcion = partes[3];
                String tarjeta = partes[4];
                String ubicacion = partes[5];
                String direccion = partes[6];
                String web = partes[7];
                String clasificacionHotel = partes[8];
                String fotoHotel = partes[9];
                String latitud = "";
                String longitud = "";
                Hotel h = new Hotel(idHotel, idCiudad, nombreHotel, descripcion, tarjeta, ubicacion, direccion, web, clasificacionHotel, fotoHotel, latitud, longitud);
                hotelesGlobales.add(h);
                line3 = br3.readLine();
            }
        }catch (IOException e) {
          System.out.println("Error");
      } finally {
         if (null!=br3) {
            br3.close();    
        }}
    }
        //Metodo que vincula los hoteles con sus servicios mediante el id del hotel
     public static void vincularServicios(ArrayList<Servicio> servicios) {
        for (Servicio s : servicios) {
            int idHot = Integer.parseInt(s.getId_hotel());
            Hotel h = get(idHot);
            h.servicios.add(s);
        }

    }
     //Metodo que vincula los hoteles con la habitacion mediante el id del hotel
      public static void vincularHabitaciones(ArrayList<Habitacion>habitaciones) {

        for (Habitacion hab : habitaciones) {
            int idH = Integer.parseInt(hab.getIdHotel());
                Hotel h = get(idH);
                h.habitaciones.add(hab);
        }
     }
      //Metodo para obtener el id del hotel
         public static Hotel get(int id) {
        return hotelesGlobales.get(id - 1);
    }
      
         
         //Getters and Setters
    public String getIdCiudad() {
        return idCiudad;
    }

    public String getIdHotel() {
        return idHotel;
    }

    public void setIdHotel(String idHotel) {
        this.idHotel = idHotel;
    }

    public String getNombreHotel() {
        return nombreHotel;
    }

    public void setNombreHotel(String nombreHotel) {
        this.nombreHotel = nombreHotel;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getClasificacionHotel() {
        return clasificacionHotel;
    }

    public void setClasificacionHotel(String clasificacionHotel) {
        this.clasificacionHotel = clasificacionHotel;
    }

    public String getFotoHotel() {
        return fotoHotel;
    }

    public void setFotoHotel(String fotoHotel) {
        this.fotoHotel = fotoHotel;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public static ArrayList<Hotel> getHotelesGlobales() {
        return hotelesGlobales;
    }

    public static void setHotelesGlobales(ArrayList<Hotel> hotelesGlobales) {
        Hotel.hotelesGlobales = hotelesGlobales;
    }

    public ArrayList<Habitacion> getHabitaciones() {
        return habitaciones;
    }

    public void setHabitaciones(ArrayList<Habitacion> habitaciones) {
        this.habitaciones = habitaciones;
    }

    public ArrayList<Servicio> getServicios() {
        return servicios;
    }

    public void setServicios(ArrayList<Servicio> servicios) {
        this.servicios = servicios;
    }
    
    
}
