/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lugar;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Municipio de Gye
 */
public class Ciudad {
    private String idProvincia;
    private String idCiudad;
    private String ciudad;
    //Esta lista contine el Id de las ciudades correspondiente a cada hotel
     public ArrayList<Hotel> hoteles = new ArrayList<>();
     //Lista con todas las ciudades 
     private static ArrayList<Ciudad> CiudadesGlobales = new ArrayList<>();

    public Ciudad(String idProvincia, String idCiudad, String ciudad) {
        this.idProvincia = idProvincia;
        this.idCiudad = idCiudad;
        this.ciudad = ciudad;
    }
    //Se lee el archivo de ciudad para poder llenar nuestra lista de ciudadesGlobales 
     public static void load() throws IOException {
        BufferedReader br2=null;
        try{
            br2=new BufferedReader(new FileReader("ciudades.csv"));    
            br2.readLine();
            String line2=br2.readLine();  
            while(null!=line2){
                String[] partesCiudades=line2.split("\\|"); 
                String idCiudades=partesCiudades[0].toLowerCase();
                String idProvincia=partesCiudades[1].toLowerCase();
                String ciudad=partesCiudades[2].toLowerCase();
                Ciudad c=new Ciudad(idCiudades,idProvincia,ciudad);
                CiudadesGlobales.add(c);
                line2 = br2.readLine();}  
        }catch (Exception e) {
          System.out.println("Error");
      } finally {
         if (null!=br2) {
            br2.close();
            }
        }
    }
     //Con este metodo lo que se procede hacer es que acada ciudad se le agrega 
     //el hotel mediante el Id
         public static void vincularHoteles(ArrayList<Hotel> hoteles) {
        for (Hotel h : hoteles) {
            int idC = Integer.parseInt(h.getIdCiudad());
                Ciudad c = get(idC);
                c.hoteles.add(h);
        }
    }
    //para obtener el indice de la ciudad
    public static Ciudad get(int index) {
        return CiudadesGlobales.get(index - 1);
    }
    //para obtener la ciudad si se ingresa el nombre de la misma 
    public static Ciudad find(String nombre) {
        for (Ciudad c : CiudadesGlobales) {
            if (c.getCiudad().equals(nombre))
                return c;
        }
        return null;
    }
    //Getters and Setters
    public String getCiudad() {
        return ciudad;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public String getIdProvincia() {
        return idProvincia;
    }

    public void setIdProvincia(String idProvincia) {
        this.idProvincia = idProvincia;
    }

    public ArrayList<Hotel> getHoteles() {
        return hoteles;
    }

    public void setHoteles(ArrayList<Hotel> hoteles) {
        this.hoteles = hoteles;
    }

    public static ArrayList<Ciudad> getCiudadesGlobales() {
        return CiudadesGlobales;
    }

    public static void setCiudadesGlobales(ArrayList<Ciudad> CiudadesGlobales) {
        Ciudad.CiudadesGlobales = CiudadesGlobales;
    }
    
    
}
